require.config({
  waitSeconds: 0,
  hbs: {
    templateExtension: 'html',
    disableHelpers: false
  },

  shim: {
    'jquery': {
      exports: '$'
    },
    'underscore': {
      exports: '_'
    },
    'backbone': {
      exports: 'Backbone',
      deps: ['underscore', 'jquery']
    },
    'marionette': {
      deps: ['backbone'],
      exports: 'Marionette'
    },
    'handlebars': {
      deps: ['handlebars'],
      exports: 'Handlebars'
    },
    d3: {
      exports: 'd3'
    },
    'tauCharts': {
      deps: ['d3', 'underscore', 'marionette', 'backbone', 'jquery'],
      exports: 'tauCharts'
    },
  },

  paths: {
    jquery: 'libs/jquery/dist/jquery.min',
    underscore: 'libs/underscore/underscore',
    backbone: 'libs/backbone/backbone',
    marionette: 'libs/marionette/lib/backbone.marionette',
    hbs: 'libs/require-handlebars-plugin/hbs',
    d3: 'libs/d3/d3.min',
    tauCharts: 'libs/taucharts/build/development/tauCharts',
    tauChartsLegend: 'libs/taucharts/build/development/plugins/tauCharts.legend',
    tauChartsTooltip: 'libs/taucharts/build/development/plugins/tauCharts.tooltip',
  },
  // map: {
  //   '*': {
  //     d3: 'libs/d3/d3.min.js',
  //     underscore: 'libs/underscore/underscore.js',
  //     tauCharts: 'libs/taucharts/build/development/tauCharts.js',
  //   }
  // },
  baseUrl: './'
});

// Entry Point
requirejs([
  'jquery',
  'backbone',
  'marionette',
  'hbs!template',
  'chart'
], function (
  $,
  Backbone,
  Marionette,
  Template,
  chart
) {
  var App = new Backbone.Marionette.Application();
  // View
  var IndexView = Backbone.Marionette.ItemView.extend({
    template: Template,
    ui: {
      chart: '#chart'
    },
    events: {},
    onShow: function () {
      console.log('onShow()');
      var that = this;
      console.log('that = this');
      var $el = that.ui.chart;
      console.log('$el defined as', $el);
      console.log('we going to plot()');
      chart($el);
      console.log('wow rendered');
    },
  });
  // Region
  App.addRegions({
    app: '#application',
  });
  App.addInitializer(function () {
    Backbone.history.start();
  });
  // Render
  App.start();
  App.app.show(new IndexView());
});
