define([
  'tauCharts',
  'tauChartsLegend',
  'tauChartsTooltip'
], function (
  tauCharts,
  legend,
  tooltip
) {
  return function ($el) {
    // Code is absolutely similar to the http://output.jsbin.com/habeza
    var plot = new tauCharts.Plot({

      settings: {
        fitModel: 'none'
      },
      plugins: [
        // tauCharts.api.plugins.get('tooltip')(),
        // tauCharts.api.plugins.get('legend')(),
        legend(),
        tooltip(),
      ],
      sources: {
        '?': {
          dims: {},
          data: []
        },

        '$': {
          dims: {
            x: {
              type: 'category'
            },
            y: {
              type: 'category'
            }
          },
          data: [{
            x: 1,
            y: 1
          }]
        },

        '/': {
          dims: {
            day: {
              type: 'category'
            },
            followers: {
              type: 'measure'
            },
            stats: {
              type: 'measure'
            },
            stage: {
              type: 'category'
            }
          },
          data: [{
            day: new Date(2015, 10, 10),
            followers: 197,
            stage: 'followers',
            stats: 0,
          }, {
            day: new Date(2015, 10, 11),
            followers: 246,
            stats: 0,
            stage: 'followers',
          }, {
            day: new Date(2015, 10, 12),
            followers: 238,
            stats: 0,
            stage: 'followers',

          }, {
            day: new Date(2015, 10, 10),
            stage: 'likes',
            stats: 20,
          }, {
            day: new Date(2015, 10, 11),
            stage: 'likes',

            stats: 19,
          }, {
            day: new Date(2015, 10, 12),
            stage: 'likes',
            stats: 2,
          }, {
            day: new Date(2015, 10, 10),
            stage: 'shares',
            stats: 5,
          }, {
            day: new Date(2015, 10, 11),
            stage: 'shares',

            stats: 1,
          }, {
            day: new Date(2015, 10, 12),
            stage: 'shares',
            stats: 10,
          }, {
            day: new Date(2015, 10, 10),
            stage: 'comments',
            stats: 2,
          }, {
            day: new Date(2015, 10, 11),
            stage: 'comments',

            stats: 4,
          }, {
            day: new Date(2015, 10, 12),
            stage: 'comments',
            stats: 5,
          }, ]
        }
      },

      scales: {

        'xScale': {
          type: 'ordinal',
          source: '$',
          dim: 'x'
        },
        'yScale': {
          type: 'ordinal',
          source: '$',
          dim: 'y'
        },

        'x_day': {
          'type': 'period',
          'period': 'day',
          'source': '/',
          'dim': 'day',
          'autoScale': true,
          'dimType': 'category'
        },
        'y_followers': {
          'type': 'linear',
          'source': '/',
          'dim': 'followers',
          'autoScale': true,
          'dimType': 'measure'
        },
        'y_stats': {
          'type': 'linear',
          'source': '/',
          'dim': 'stats',
          'autoScale': true,
          'dimType': 'measure'
        },
        'color_undefined': {
          'type': 'color',
          'source': '/'
        },
        'color_undefined2': {
          'type': 'color',
          'source': '/',
          'dim': 'stage'
        },
        color_line: {
          type: 'color',
          source: '/',
          //dim: 'followers'
        },
        'stacked_c1': {
          'type': 'color',
          'source': '/',
          'dim': 'stats'
        },
        'size_undefined': {
          'type': 'size',
          'source': '/',
          'min': 2,
          'max': 10,
          'mid': 5
        }
      },
      transformations: {
        'exclude-empty': function (data, props) {
          return _(data).filter(function (it) {
            return it.followers;
          });
        }
      },
      unit: {
        type: 'COORDS.RECT',
        x: 'xScale',
        y: 'yScale',
        expression: {
          source: '$',
          inherit: false,
          operator: false
        },
        guide: {},
        frames: [{
          key: {
            x: 1,
            y: 1,
            i: 1
          },
          source: '$',
          pipe: [],
          units: [{
            x: 'x_day',
            y: 'y_stats',
            type: 'COORDS.RECT',
            expression: {
              inherit: false,
              operator: 'none',
              params: [],
              source: '/'
            },
            guide: {
              autoLayout: '',
              x: {
                autoScale: true,
                cssClass: 'x axis',
                hide: true,
                label: {
                  cssClass: 'label',
                  padding: 129,
                  rotate: 0,
                  size: 889.75,
                  text: 'day',
                  textAnchor: 'middle'
                },
                padding: 20,
                rotate: 90,
                scaleOrient: 'bottom',
                textAnchor: 'start'
              },
              y: {
                autoScale: true,
                cssClass: 'y axis',
                hide: false,
                label: {
                  cssClass: 'label',
                  // padding: 70.25,
                  padding: 30,
                  rotate: -90,
                  size: 431,
                  text: 'likes, shares, comments',
                  textAnchor: 'middle'
                },
                padding: 65,
                rotate: 0,
                scaleOrient: 'left',
                textAnchor: 'end',
                tickFormat: 'x-num-auto'
              },
              padding: {
                b: 159,
                l: 110.25,
                r: 10,
                t: 10
              },
              showGridLines: 'y'
            },

            units: [{
              size: 'size_undefined',
              type: 'ELEMENT.INTERVAL.STACKED',
              x: 'x_day',
              y: 'y_stats',
              color: 'color_undefined2',
              expression: {
                inherit: false,
                operator: 'none',
                params: [],
                source: '/'
              },
              guide: {
                flip: true,
                anchors: false,
                cssClass: 'i-role-datum',
                showGridLines: 'xy',
                widthCssClass: '',
                x: {
                  tickFontHeight: 10
                },
                y: {
                  tickFontHeight: 10
                }
              }
            }]
          }]
        }, {
          key: {
            x: 1,
            y: 1,
            i: 0
          },
          source: '$',
          pipe: [],
          units: [{
            x: 'x_day',
            y: 'y_followers',
            type: 'COORDS.RECT',
            expression: {
              inherit: false,
              operator: 'none',
              params: [],
              source: '/'
            },
            guide: {
              autoLayout: '',
              x: {
                autoScale: true,
                cssClass: 'x axis',
                hide: false,
                label: {
                  cssClass: 'label',
                  padding: 129,
                  rotate: 0,
                  size: 889.75,
                  text: 'day',
                  textAnchor: 'middle'
                },
                padding: 20,
                rotate: 90,
                scaleOrient: 'bottom',
                textAnchor: 'start'
              },
              y: {
                autoScale: true,
                cssClass: 'y axis',
                hide: false,
                label: {
                  cssClass: 'label',
                  padding: 30,
                  rotate: -90,
                  size: 431,
                  text: 'followers',
                  textAnchor: 'middle'
                },
                padding: 23,
                rotate: 0,
                scaleOrient: 'left',
                textAnchor: 'end',
                tickFormat: 'x-num-auto'
              },
              padding: {
                b: 159,
                l: 110.25,
                r: 10,
                t: 10
              },
              showGridLines: ''
            },

            units: [{
              size: 'size_undefined',
              type: 'ELEMENT.LINE',
              x: 'x_day',
              y: 'y_followers',
              color: 'color_line',
              transformation: [{
                type: 'exclude-empty'
              }],
              expression: {
                inherit: false,
                operator: 'none',
                params: [],
                source: '/'
              },
              guide: {
                anchors: false,
                cssClass: 'i-role-datum',
                showGridLines: 'xy',
                widthCssClass: ''
              }
            }]
          }]
        }, ]
      }
    });

    plot.renderTo($el[0]);
    plot.on('elementclick', function (chartRef, e) {
      console.log(e.data);
      // alert(JSON.stringify(e.data));
    });
  };
});
