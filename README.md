\w v0.6.2 (production version):
Screenshot: http://prntscr.com/8u26qw
```
Uncaught TypeError: Cannot read property 'extractFieldsFormatInfo' of undefined
  Tooltip.plugin._getFormatters @ tauCharts.tooltip.js:379
  Tooltip.plugin.onRender @ tauCharts.tooltip.js:277
  b @ tauCharts.min.js:4
  d.value @ tauCharts.min.js:4
  x.value @ tauCharts.min.js:6
  (anonymous function) @ chart.js:380
  Backbone.Marionette.ItemView.extend.onShow @ main.js:86
  ...
```

\w v0.6.2 (development version):
Screenshot: http://prntscr.com/8u2715
```
Uncaught TypeError: Cannot read property 'type' of undefined
  createScaleInfo @ tauCharts.js:3982
  (anonymous function) @ tauCharts.js:3075
  createScales @ tauCharts.js:1244
  _drawUnitsStructure @ tauCharts.js:3073
  ...
```

Version 0.6.0 has similar problem, although v0.5.2 works fine.
